﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace MvvmDemo.ViewModels
{
    class PlaylistViewModel : BaseViewModel
    {
        public string Title { get; set; }

        private bool _isFavorite;
        public bool IsFavorite
        {
            get { return _isFavorite; }
            set
            {
                if (_isFavorite == value)
                    return;

                _isFavorite = value;

                OnPropertyChanged();

                OnPropertyChanged(nameof(Color));
            }
        }

        public Color Color
        {
            get { return IsFavorite ? Color.Pink : Color.Black; }
        }
    }
}
