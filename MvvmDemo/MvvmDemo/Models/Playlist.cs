﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MvvmDemo.Models
{
    class Playlist
    {
        public string Title { get; set; }
        public bool IsFavorite { get; set; }
    }
}
